package factoryMethod.Transport;

public class Tank implements CargoTransport {
    @Override
    public void process() {
        // metoda
        System.out.print("\nW razie dzialan wojennych,");
        System.out.print("\npoczekaj na stabilizacje");
        System.out.print("\nw kraju...\n");
    }
}
